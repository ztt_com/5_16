package com.qf.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 高圆圆
 * @date 2023/5/16 16:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResult {
    private Integer code ;
    private String msg ;
    private Object data ;
    private Integer count ;
}
