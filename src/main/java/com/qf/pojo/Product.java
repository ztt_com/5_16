package com.qf.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者：涛
 * 时间：2023/5/16 21:05
 * 描述：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    private Integer id;//商品编号
    private String name;//商品名称
    private Integer price;//商品价格
    private String desc;//商品描述
}
