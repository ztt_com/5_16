package com.qf.controller;

import com.qf.pojo.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：涛
 * 时间：2023/5/16 22:25
 * 描述：
 */
@Controller
@RequestMapping("/product")
public class ProductController {
    private  static List<Product> list = new ArrayList<>();
    static  {
        list.add(new Product(1,"农夫山泉",2,"一人一拳"));
        list.add(new Product(2,"百事可乐",3,"肥仔快乐水"));
        list.add(new Product(3,"可口可乐",3,"肥仔快乐水"));
        list.add(new Product(4,"芬达",3,"摇起来"));
    }

    @RequestMapping("/findAll")
    public String findAllProduct(Model model){
       model.addAttribute("list",list);
        return "product_list";
    }
    @RequestMapping("/addProduct")
    public ModelAndView addProduct(Product product){
        list.add(product);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("mussic","添加成功！");
        modelAndView.setViewName("add_product");
        return modelAndView;
    }
    @RequestMapping("/deleteProduct")
    public String deleteProduct(Integer id){

        for (int x = 0;x<list.size();x++){
            if (list.get(x).getId().equals(id)){
                list.remove(x);
            }
        }
        return "forward:/product/findAll";
    }
    @RequestMapping("/updateProduct")
    public ModelAndView updateProduct(Product product){

        for (int x = 0;x<list.size();x++){
            if (list.get(x).getId().equals(product.getId())){

                Product p = list.get(x);
                p.setName(product.getName());
                p.setPrice(product.getPrice());
                p.setDesc(product.getDesc());
            }
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("mussic","修改成功!");
        modelAndView.setViewName("update_product");

        return modelAndView;
    }
}
