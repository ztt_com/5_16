<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2023/5/17
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加商品</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/product/addProduct" method="post">
  <div>
    <label for="id">商品编号</label>
    <input type="text" name="id" id="id" placeholder="请输入商品编号">
  </div>
  <div>
    <label for="name">商品名称</label>
    <input type="text" name="name" id="name" placeholder="请输入商品名称">
  </div>
  <div>
    <label for="price">商品价格</label>
    <input type="text" name="price" id="price" placeholder="请输入商品价格">
  </div>
  <div>
    <label for="desc">商品描述</label>
    <input type="text" name="desc" id="desc" placeholder="请输入商品描述">
  </div>
  <input type="submit" value="添加">
  <a href="${pageContext.request.contextPath}/product/findAll">
    <input type="button" value="返回">
  </a>
  </div>
</form>
<div>${mussic}</div>
</body>
</html>
