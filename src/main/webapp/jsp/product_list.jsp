<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2023/5/16
  Time: 22:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>商品列表页面</title>
</head>
<body>
<table border="2px" width="500px" height="500px" cellspacing="0" align="center">
    <a href="/day5_16_war_exploded/jsp/add_product.jsp">添加商品</a>
    <tr>
        <th>商品编号</th>
        <th>商品名称</th>
        <th>商品价格</th>
        <th>商品描述</th>
        <th>操作</th>
    </tr>

        <c:forEach items="${list}" var="p">
            <tr>
                <td>${p.id}</td>
                <td>${p.name}</td>
                <td>${p.price}</td>
                <td>${p.desc}</td>
                <td>
                    <a href="/day5_16_war_exploded/jsp/update_product.jsp">修改</a>
                    <a href="${pageContext.request.contextPath}/product/deleteProduct?id=${p.id}">删除</a>
                </td>
            </tr>
        </c:forEach>
</table>

</body>

</html>
